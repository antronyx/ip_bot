FROM debian:stretch
RUN apt-get update
RUN apt-get install -y python3-pip
WORKDIR /app
COPY requirements.txt /app/
COPY main.py /app/
RUN pip3 install --upgrade pip
RUN pip3 install -r requirements.txt
CMD python3 main.py
